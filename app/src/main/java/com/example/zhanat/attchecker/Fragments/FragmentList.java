package com.example.zhanat.attchecker.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zhanat.attchecker.Adapters.AdapterStudent;
import com.example.zhanat.attchecker.Constructers.ConstructerStudent;
import com.example.zhanat.attchecker.R;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentList extends Fragment {

    ArrayList<ConstructerStudent> constructerStudents = new ArrayList<ConstructerStudent>();
    AdapterStudent adapterStudent;
    FragmentMain fragmentMain;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    TextView textView7, textView10, textView12;
    Button button5;

    Firebase ref;
    String surname;
    String name;
    String group;
    String groupID;
    String groupIDKey;
    int count;
    int counter;

    ListView listView;

    public FragmentList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Firebase.setAndroidContext(getContext());
        ref = new Firebase("https://blistering-heat-528.firebaseio.com");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        sharedPreferences = getContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        textView7 = (TextView) view.findViewById(R.id.textView7);
        textView10 = (TextView) view.findViewById(R.id.textView10);
        textView12 = (TextView) view.findViewById(R.id.textView12);
        button5 = (Button) view.findViewById(R.id.button5);

        textView7.setText(sharedPreferences.getString("groupIDKey",""));
        textView10.setText(sharedPreferences.getString("surnameKey",""));
        textView12.setText(sharedPreferences.getString("nameKey",""));

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sharedPreferences.edit().clear().commit();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentMain = new FragmentMain();
                fragmentTransaction.replace(R.id.container, fragmentMain).commit();
            }
        });


        return view;
    }

    @Override
    public void onResume() {

        sharedPreferences = getContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        groupIDKey = sharedPreferences.getString("groupIDKey","");
        ref.child("counting").child(groupIDKey).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                constructerStudents.removeAll(constructerStudents);
                count = Integer.parseInt((String) dataSnapshot.child("count").getValue());
                for(int i=1; i<count;i++)
                {
                    ref.child("students").child(groupIDKey).child(i + "").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            name = (String) snapshot.child("name").getValue();
                            surname = (String) snapshot.child("surname").getValue();
                            group = (String) snapshot.child("group").getValue();
                            groupID = (String) snapshot.child("groupID").getValue();

                            constructerStudents.add(new ConstructerStudent(group, surname, name, groupID,""));
                            adapterStudent = new AdapterStudent(getContext(), constructerStudents);
                            listView.setAdapter(adapterStudent);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        super.onResume();
    }

}

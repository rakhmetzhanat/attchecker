package com.example.zhanat.attchecker.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zhanat.attchecker.Adapters.AdapterStudent;
import com.example.zhanat.attchecker.Constructers.ConstructerStudent;
import com.example.zhanat.attchecker.R;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTeacher extends Fragment {

    FragmentMain fragmentMain;

    TextView textView3,textView4;
    Button button3;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";

    private String dateKey;
    private String loginKey;
    private Firebase ref;

    ArrayList<ConstructerStudent> constructerStudents = new ArrayList<ConstructerStudent>();
    AdapterStudent adapterStudent;

    String surname;
    String name;
    String group;
    String groupID;
    String groupIDKey;
    int count;
    int counter;

    ListView listView;

    public FragmentTeacher() {
        // Required empty public constructor
    }

    public static String RandomNumber(int size){
        String chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        String ret = "";
        int length = chars.length();
        for (int i = 0; i <=size; i ++){
            ret += chars.split("")[ (int) (Math.random() * (length - 1)) ];
        }
        return ret;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher, container, false);
        Firebase.setAndroidContext(getContext());
        ref = new Firebase("https://blistering-heat-528.firebaseio.com");

        textView3 = (TextView) view.findViewById(R.id.textView3);
        textView4 = (TextView) view.findViewById(R.id.textView4);
        button3 = (Button) view.findViewById(R.id.button3);

        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


        loginKey = sharedPreferences.getString("loginKey","");

        if(loginKey.equals("true"))
        {
            groupIDKey = sharedPreferences.getString("groupIDKey","");
            dateKey = sharedPreferences.getString("dateKey","");
        }else
        {
            groupIDKey = RandomNumber(6);
            dateKey = sdf.format(c.getTime());

            editor.putString("groupIDKey", groupIDKey);
            editor.putString("dateKey", dateKey);
            editor.putString("loginKey", "true");
            editor.commit();

            ref.child("counting").child(groupIDKey).child("count").setValue("1");
            Toast toast = Toast.makeText(getContext() ,"Регистрация успешно завершена!", Toast.LENGTH_SHORT);
            toast.show();
        }

        textView3.setText(sharedPreferences.getString("groupIDKey",""));
        textView4.setText(sharedPreferences.getString("dateKey",""));

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sharedPreferences.edit().clear().commit();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentMain = new FragmentMain();
                fragmentTransaction.replace(R.id.container, fragmentMain).commit();
            }
        });

        listView = (ListView) view.findViewById(R.id.listView2);

        return view;
    }



    @Override
    public void onResume() {


        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        groupIDKey = sharedPreferences.getString("groupIDKey","");
        ref.child("counting").child(groupIDKey).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                constructerStudents.removeAll(constructerStudents);
                count = Integer.parseInt((String) dataSnapshot.child("count").getValue());
                for(int i=0; i<count;i++)
                {
                    counter = i;
                    ref.child("students").child(groupIDKey).child(counter + "").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            name = (String) snapshot.child("name").getValue();
                            surname = (String) snapshot.child("surname").getValue();
                            group = (String) snapshot.child("group").getValue();
                            groupID = (String) snapshot.child("groupID").getValue();

                            constructerStudents.add(new ConstructerStudent(group, surname, name, groupID,""));
                            adapterStudent = new AdapterStudent(getContext(), constructerStudents);
                            listView.setAdapter(adapterStudent);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        super.onResume();
    }



}

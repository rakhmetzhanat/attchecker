package com.example.zhanat.attchecker.Adapters;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.zhanat.attchecker.Constructers.ConstructerStudent;
import com.example.zhanat.attchecker.R;



/**
 * Created by zhanat on 4/26/16.
 */
public class AdapterStudent extends BaseAdapter{

    TextView textView14, textView15, textView16, textView18, textView19;
    ArrayList<ConstructerStudent> objects;
    LayoutInflater lInflater;
    Context ctx;

    public AdapterStudent(Context context, ArrayList<ConstructerStudent> students) {
        ctx = context;
        objects = students;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = lInflater.inflate(R.layout.items, parent, false);
        if (view == null) {
            view = lInflater.inflate(R.layout.items, parent, false);
        }

        ConstructerStudent constructerStudent = getConstructerStudent(position);

        textView14 = (TextView) view.findViewById(R.id.textView14);
        textView14.setText(constructerStudent.group);

        textView15 = (TextView) view.findViewById(R.id.textView15);
        textView15.setText(constructerStudent.surname);

        textView16 = (TextView) view.findViewById(R.id.textView16);
        textView16.setText(constructerStudent.name);

        textView18 = (TextView) view.findViewById(R.id.textView18);
        textView18.setText(constructerStudent.groupID);

        textView19 = (TextView) view.findViewById(R.id.textView19);
        textView19.setText(constructerStudent.count);

        return view;
    }

    ConstructerStudent getConstructerStudent(int position) {
        return ((ConstructerStudent) getItem(position));
    }
}

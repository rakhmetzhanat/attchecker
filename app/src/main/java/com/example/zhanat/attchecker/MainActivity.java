package com.example.zhanat.attchecker;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.zhanat.attchecker.Fragments.FragmentList;
import com.example.zhanat.attchecker.Fragments.FragmentMain;
import com.example.zhanat.attchecker.Fragments.FragmentStudent;
import com.example.zhanat.attchecker.Fragments.FragmentTeacher;
import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity {

    FragmentMain fragmentMain;
    FragmentTeacher fragmentTeacher;
    FragmentList fragmentList;


    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private String userKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Firebase.setAndroidContext(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        userKey = sharedPreferences.getString("userKey","");

        if(userKey.equals("teacher"))
        {
            if(savedInstanceState == null)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTeacher = new FragmentTeacher();
                fragmentTransaction.replace(R.id.container, fragmentTeacher).commit();
            }
        }
        else if(userKey.equals("student"))
        {
            if(savedInstanceState == null)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentList = new FragmentList();
                fragmentTransaction.replace(R.id.container, fragmentList).commit();
            }
        }else
        {
            if(savedInstanceState == null)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentMain = new FragmentMain();
                fragmentTransaction.replace(R.id.container, fragmentMain).commit();
            }
        }


    }
}

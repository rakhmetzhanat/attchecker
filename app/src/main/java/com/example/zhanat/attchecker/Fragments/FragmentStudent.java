package com.example.zhanat.attchecker.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.zhanat.attchecker.R;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentStudent extends Fragment {

    private Button RegButton;
    private EditText surname, name, groupID, group;
    public int count;
    private Firebase ref;
    FragmentList fragmentList;
    FragmentMain fragmentMain;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";

    public FragmentStudent() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Firebase.setAndroidContext(getContext());
        ref = new Firebase("https://blistering-heat-528.firebaseio.com");
        View view = inflater.inflate(R.layout.fragment_student, container, false);

        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        surname = (EditText) view.findViewById(R.id.editText);
        name = (EditText) view.findViewById(R.id.editText2);
        groupID = (EditText) view.findViewById(R.id.editText3);
        group = (EditText) view.findViewById(R.id.editText4);


        RegButton = (Button) view.findViewById(R.id.button4);

        RegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        return view;
    }

    private void attemptLogin() {

        surname.setError(null);
        name.setError(null);
        groupID.setError(null);
        group.setError(null);

        final String surname_str = surname.getText().toString();
        final String name_str = name.getText().toString();
        final String groupID_str= groupID.getText().toString();
        final String group_str = group.getText().toString();


        if(TextUtils.isEmpty(surname_str))
        {
            surname.setError("Пустое поле");
        }
        else if (TextUtils.isEmpty(name_str)) {
            name.setError("Пустое поле");
        }
        else if (TextUtils.isEmpty(group_str)) {
            group.setError("Пустое поле");
        }
        else if (TextUtils.isEmpty(groupID_str)) {
            groupID.setError("Пустое поле");
        }
        else if (!isPasswordValid(groupID_str)) {
            groupID.setError("Не менее 6 элементов");
        }else
        {

            ref.child("counting").child(groupID_str).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if((String) snapshot.child("count").getValue()!=null)
                    {
                        count = Integer.parseInt((String) snapshot.child("count").getValue());

                        Map<String, String> students = new HashMap<String, String>();

                        students.put("surname",surname_str);
                        students.put("name", name_str);
                        students.put("groupID", groupID_str);
                        students.put("group", group_str);
                        ref.child("students").child(groupID_str).child(count + "").setValue(students);
                        ref.child("counting").child(groupID_str).child("count").setValue((count + 1) + "");

                        editor.putString("surnameKey", surname_str);
                        editor.putString("nameKey", name_str);
                        editor.putString("groupIDKey", groupID_str);
                        editor.putString("groupKey", group_str);
                        editor.putString("userKey", "student");

                        editor.commit();

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentList = new FragmentList();
                        fragmentTransaction.replace(R.id.container, fragmentList).commit();

                        Toast toast = Toast.makeText(getContext() ,"Регистрация успешно завершена!", Toast.LENGTH_SHORT);
                        toast.show();
                    }else
                    {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentMain = new FragmentMain();
                        fragmentTransaction.replace(R.id.container, fragmentMain).commit();
                    }

                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });

        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 5;
    }

}

package com.example.zhanat.attchecker.Constructers;

import android.widget.TextView;

/**
 * Created by zhanat on 4/26/16.
 */
public class ConstructerStudent {

    public String surname;
    public String name;
    public String groupID;
    public String group;
    public String count;
    public ConstructerStudent(String groups, String surnames, String names, String groupIDs, String counts) {
        surname = surnames;
        name = names;
        groupID = groupIDs;
        group = groups;
        count = counts;
    }
}
